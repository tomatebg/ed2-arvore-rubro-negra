#include <stdio.h>

#include <stdlib.h>

#define RED 1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB * esq;
  struct arvoreRB * dir;
}
ArvoreRB;

int eh_no_vermelho(ArvoreRB * no) {
  if (!no) return BLACK;
  return (no -> cor == RED);
}

int buscar(ArvoreRB * a, int v) {
  if (a == NULL) {
    return 0;
  } else if (v < a -> info) {
    return buscar(a -> esq, v);
  } else if (v > a -> info) {
    return buscar(a -> dir, v);
  } else {
    return 1;
  }
}

void troca_cor(ArvoreRB * a) {
  a -> cor = !a -> cor;
  if (a -> esq != NULL) a -> esq -> cor = !a -> esq -> cor;
  if (a -> dir != NULL) a -> dir -> cor = !a -> dir -> cor;

}

ArvoreRB * rotaciona_direita(ArvoreRB * a) {
  ArvoreRB * b = a -> esq;
  a -> esq = b -> dir;
  b -> dir = a;
  b -> cor = a -> cor;
  a -> cor = RED;
  return b;
}

ArvoreRB * rotaciona_esquerda(ArvoreRB * a) {
  ArvoreRB * b = a -> dir;
  a -> dir = b -> esq;
  b -> esq = a;
  b -> cor = a -> cor;
  a -> cor = RED;
  return b;
}

ArvoreRB * move_direita_red(ArvoreRB * H) {
  troca_cor(H);
  if (eh_no_vermelho(H -> esq -> esq) == RED) {
    H = rotaciona_direita(H);
    troca_cor(H);
  }
  return H;
}

ArvoreRB * move_esquerda_red(ArvoreRB * H) {
  troca_cor(H);
  if (eh_no_vermelho(H -> dir -> esq) == RED) {
    H -> dir = rotaciona_esquerda(H);
    troca_cor(H);
  }
  return H;
}

ArvoreRB * balancear(ArvoreRB * H) {
  //n� Vermelho filho � esquerda
  if (eh_no_vermelho(H -> dir) == RED)
    H = rotaciona_esquerda(H);

  //Filho da esquerda e neto da esquerda vermelhos

  if (H -> esq != NULL && eh_no_vermelho(H -> esq) == RED && eh_no_vermelho(H -> esq -> esq) == RED)
    H = rotaciona_direita(H);

  //seguidos vermelhos
  if (eh_no_vermelho(H -> esq) == RED && eh_no_vermelho(H -> dir) == RED)
    troca_cor(H);

  return H;
}

ArvoreRB * inserir(ArvoreRB * a, int v) {
  if (a == NULL) {
    a = (ArvoreRB * ) malloc(sizeof(ArvoreRB));
    a -> info = v;
    a -> esq = a -> dir = NULL;
  } else if (v < a -> info) {
    a -> esq = inserir(a -> esq, v);
  } else {
    a -> dir = inserir(a -> dir, v);
  }

  if (eh_no_vermelho(a -> dir) == RED && eh_no_vermelho(a -> esq) == BLACK)
    a = rotaciona_esquerda(a);

  if (eh_no_vermelho(a -> esq) == RED && eh_no_vermelho(a -> esq -> esq) == RED)
    a = rotaciona_direita(a);

  if (eh_no_vermelho(a -> esq) == RED && eh_no_vermelho(a -> dir) == RED)
    troca_cor(a);

  return a;

}

void in_order(ArvoreRB * a) {
  if (!a)
    return;
  in_order(a -> esq);
  printf("%d ", a -> info);
  in_order(a -> dir);
}

ArvoreRB * remove_menor_esquerda(ArvoreRB * H) {
  if (H -> esq == NULL) {
    free(H);
    return NULL;
  }
  if (eh_no_vermelho(H -> esq) == BLACK && eh_no_vermelho(H -> esq -> esq) == BLACK)
    H = move_esquerda_red(H);

  H -> esq = remove_menor_esquerda(H -> esq);
  balancear(H);
}

ArvoreRB * consulta_menor_esquerda(ArvoreRB * atual) {
  ArvoreRB * no1 = atual;
  ArvoreRB * no2 = atual -> esq;
  while (no2 != NULL) {
    no1 = no2;
    no2 = no2 -> esq;
  }
  return no1;
}

ArvoreRB * remover(ArvoreRB * H, int valor) {
  if (valor < H -> info) {
    if (eh_no_vermelho(H -> esq) == BLACK && eh_no_vermelho(H -> esq -> esq) == BLACK)
      H = move_esquerda_red(H);

    H -> esq = remover(H -> esq, valor);
  } else {
    if (eh_no_vermelho(H -> esq) == RED)
      H = rotaciona_direita(H);

    if (valor == H -> info && (H -> dir == NULL)) {
      free(H);
      return NULL;
    }

    if (eh_no_vermelho(H -> dir) == BLACK && eh_no_vermelho(H -> dir -> esq) == BLACK)
      H = move_direita_red(H);

    if (valor == H -> info) {
      ArvoreRB * x = consulta_menor_esquerda(H -> dir);
      H -> info = x -> info;
      H -> dir = remove_menor_esquerda(H -> dir);
    } else
      H -> dir = remover(H -> dir, valor);
  }
  if (H != NULL) H -> cor = BLACK;
  return balancear(H);
}

void print(ArvoreRB * a, int spaces) {
  int i;
  for (i = 0; i < spaces; i++) printf(" ");
  if (!a) {
    printf("//\n");
    return;
  }
  printf("%d\n", a -> info);
  print(a -> esq, spaces + 2);
  print(a -> dir, spaces + 2);
}

int main() {
  ArvoreRB * a;
  a = inserir(NULL, 50);
  a = inserir(a, 30);
  a = inserir(a, 90);
  a = inserir(a, 20);
  a = inserir(a, 40);
  a = inserir(a, 95);
  a = inserir(a, 10);
  a = inserir(a, 35);
  a = inserir(a, 45);
  a = inserir(a, 37);

  in_order(a);
  printf("\n");
  print(a, 0);
  printf("\n");
  a = remover(a, 50);
  in_order(a);
  printf("\n");
  print(a, 0);
}
